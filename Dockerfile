FROM alpine:latest

# Data management
RUN apk add \
    exfatprogs \
    dosfstools \
    e2fsprogs \
    btrfs-progs \
    xfsprogs \
    parted \
    cfdisk \
    rsync \
    rclone \
    nfs-utils \
    unzip \
    7zip \
    samba-client \
    tree \
    fdupes \
    ncdu

# Diagnostic
RUN apk add \
    htop \
    iotop \
    bind-tools \
    netcat-openbsd \
    iperf \
    iperf3 \
    strace \
    iftop \
    tcpdump

# Other tools
RUN apk add \
    bash \
    zsh \
    git \
    vim \
    curl \
    jq

# Install kubectl
RUN curl -L https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl -o /usr/bin/kubectl
RUN chmod +x /usr/bin/kubectl

ENTRYPOINT ["sleep", "infinity"]
